# gitea-rust-sdk

An unofficial rust-sdk for gitea

# Implemented api calls

For further documentation please have a look at https://codeberg.org/api/swagger or the documentation of your gitea instance.

## admin

- [ ] `GET		/admin/cron` - List cron tasks
- [ ] `POST		/admin/cron/{task}` - Run cron task
- [ ] `GET		/admin/orgs` - List all organisations
- [ ] `GET		/admin/unadopted` - List unadopted organisations
- [ ] `POST		/admin/unadopted/{owner}/{repo}` - Adopt unadopted files as a repository
- [ ] `DELETE	/admin/unadopted/{owner}/{repo}` - Delete unadopted files
- [ ] `GET		/admin/users` - List all users
- [ ] `POST		/admin/users` - Create a user
- [ ] `DELETE	/admin/users/{username}` - Delete a user
- [ ] `PATCH	/admin/users/{username}` - Edit an existing user
- [ ] `POST		/admin/users/{username}/keys` - Add a public key on behalf of a user
- [ ] `DELETE	/admin/users/{usernam}/keys/{id}` - Delete a user's public key
- [ ] `POST		/admin/users/{username}/orgs` - Create an organisation
- [ ] `POST		/admin/users/{username}/repos` - Create a repository on behalf of a user

## miscellaneous

- [ ] `POST		/markdown` - Render a markdown document as HTML
- [ ] `POST		/markdown/raw` - Render raw markdown as HTML
- [ ] `GET		/nodeinfo` - Returns the nodeinfo of the Gitea application
- [ ] `GET		/signing-key.gpg` - Get default signing-key.gpg
- [x] `GET		/version` - Returs the version of the Gitea application

## notification

- [ ] `GET		/notifications` - List users's notification threads
- [ ] `PUT		/notifications` - Mark notification threads as read, pinned or unread
- [ ] `GET		/notifications/new` - Check if unread notifications exist
- [ ] `GET		/notifications/threads/{id}` - Get notification thread by ID
- [ ] `PATCH	/notifications/threads/{id}` - Mark notification thread as read by ID
- [ ] `GET		/repos/{owner}/{repo}/notifications` - List users's notification threads on a specific repo
- [ ] `PUT		/repos/{owner}/{repo}/notifications` - Mark notification threads as read, pinned or unread on a specific repo

## organisation

- [ ] `GET		/orgs` - Get list of organisations
- [ ] `POST		/orgs` - Create an organisation
- [ ] `GET		/orgs/{org}` - Get an organisation
- [ ] `DELETE	/orgs/{org}` -  Delete an organisation
- [ ] `PATCH	/orgs/{org}` - Edit an organisation
- [ ] `GET		/orgs/{org}/hooks` - List organisation's webhooks
- [ ] `POST		/orgs/{org}/hooks` - Create a hook
- [ ] `GET		/orgs/{org}/hooks/{id}` - Get a hook
- [ ] `DELETE	/orgs/{org}/hooks/{id}` - Delete a hook
- [ ] `PATCH	/orgs/{org}/hooks/{id}` - Update a hook
- [ ] `GET		/orgs/{org}/labels` - List an organisation's labels
- [ ] `POST		/orgs/{org}/labels` - Create a label for an organisation
- [ ] `GET		/orgs/{org}/labels/{id}` - Get a single label
- [ ] `DELETE	/orgs/{org}/labels/{id}` - Delete a label
- [ ] `PATCH	/orgs/{org}/labels/{id}` - Update a label
- [ ] `GET		/orgs/{org}/members` - List an organisation's members
- [ ] `GET		/orgs/{org}/members/{username}` - Check if a user is a member of an organisation
- [ ] `DELETE	/orgs/{org}/members/{username}` - Remove a member from an organisation
- [ ] `GET		/orgs/{org}/public_members` - List an organisation's public members
- [ ] `GET		/orgs/{org}/public_members/{username}` - Check if a user is a public member of an organisation
- [ ] `PUT		/orgs/{org}/public_members/{username}` - Publicize a user's membership
- [ ] `DELETE	/orgs/{org}/public_members/{username}` - Conceal a user's membership
- [ ] `GET		/orgs/{org}/repos` - List an organisation's repos
- [ ] `POST		/orgs/{org}/repos` - Create a repository in an organisation
- [ ] `GET		/orgs/{org}/teams` - List an organisation's teams
- [ ] `POST		/orgs/{org}/teams` - Create a team
- [ ] `GET		/orgs/{org}/teams/search` - Search for teams within an organisation
- [ ] `GET		/teams/{id}` - Get a team
- [ ] `DELETE	/teams/{id}` - Delete a team
- [ ] `PATCH	/teams/{id}` - Edit a team
- [ ] `GET		/teams/{id}/members` - List a team's members
- [ ] `GET		/teams/{id}/members/{username}` - List a particular member of a team
- [ ] `PUT		/teams/{id}/members/{username}` - Add a team member
- [ ] `DELETE	/teams/{id}/members/{username}` - Remove a team member
- [ ] `GET		/teams/{id}/repos` - List a team's repos
- [ ] `GET		/teams/{id}/repos/{org}/{repo}` - List a particular repo of a team
- [ ] `PUT		/teams/{id}/repos/{org}/{repo}` - Add a repository to a team
- [ ] `DELETE	/teams/{id}/repos/{org}/{repo}` - Remove a repository from a team
- [ ] `GET		/users/orgs` - List the current user's organisations
- [ ] `GET		/users/{username}/orgs` - List a user's organisations
- [ ] `GET		/users/{username}/orgs/{org}/permissions` - Get users permissions in organisation

## package

- [ ] `GET		/packages/{owner}` - Get all packages of an owner
- [ ] `GET		/packages/{owner}/{type}/{name}/{version}` - Gets a package
- [ ] `DELETE	/packages/{owner}/{type}/{name}/{version}` - Delete a package
- [ ] `GET		/packages/{owner}/{type}/{name}/{version}/files` - Gets all files of a package

## issue

- [ ] `GET		/repos/issues/search` - Search for issues across the repository that the user has access to
- [ ] `GET		/repos/{owner}/{repo}/issues` - List a repository's issues
- [ ] `POST		/repos/{owner}/{repo}/issues` - Create an issue. If using deadline noly the date will be taken into account, and time of day ignored
- [ ] `GET		/repos/{owner}/{repo}/issues/comments` - List all comments in a repository
- [ ] `GET		/repos/{owner}/{repo}/issues/comments/{id}` - Get a comment 
- [ ] `DELETE	/repos/{owner}/{repo}/issues/comments/{id}` - Delete a comment 
- [ ] `PATCH	/repos/{owner}/{repo}/issues/comments/{id}` - Edit a comment 
- [ ] `GET		/repos/{owner}/{repo}/issues/commets/{id}/reactions` - Get a list of reactions from a comment of an issue 
- [ ] `POST		/repos/{owner}/{repo}/issues/comments/{id}/reactions` - Add a reaction to a comment of an issue 
- [ ] `DELETE	/repos/{owner}/{repo}/issues/comments/{id}/reactions` - Remove a reaction from a comment of an issue 
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}` - Get an issue 
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}` - Delete an issue 
- [ ] `PATCH	/repos/{owner}/{repo}/issues/{index}` - Edit an issue. If using deadline only the date will be taken into accound, and time of day ignored. 
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/comments` - List all comments on an issue 
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/comments` - Add a comment to an issue 
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/deadline` - Set an issue deadline. If set to null, the deadline is deleted. If using deadline only the date will be taken into account, and time of day ignored. 
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/labels` - Get an issue's labels 
- [ ] `PUT		/repos/{owner}/{repo}/issues/{index}/labels` - Replace an issue's labels 
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/labels` - Add a label to an issue
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/labels` - Remove all labels from an issue
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/labels/{id}` - Remove a label from an issue 
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/reactions` - Get a list of reactions of an issue
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/reactions` - Add a reaction to an issue 
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/reactions` - Remove a reaction from an issue 
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/stopwatch/delete` - Delete an issue's existing stopwatch 
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/stopwatch/start` - Start stopwatch of an issue 
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/stopwatch/stop` - Stop an issue's existing stopwatch 
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/subscriptions` - Get users who subscribed to an issue
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/subscriptions/check` - Check if a user is subscribed to an issue
- [ ] `PUT		/repos/{owner}/{repo}/issues/{index}/subscriptions/{user}` - Subscribe user to issue
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/subscriptions/{user}` - Unsubscribe user from issue
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/timeline` - List all comments and events on an issue
- [ ] `GET		/repos/{owner}/{repo}/issues/{index}/times` - List an issue's tracked times 
- [ ] `POST		/repos/{owner}/{repo}/issues/{index}/times` - Add tracked time to an issue
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/times` - Reset a tracked time of an issue
- [ ] `DELETE	/repos/{owner}/{repo}/issues/{index}/times/{id}` - Delete specific tracked time
- [ ] `GET		/repos/{owner}/{repo}/labels` - Get all of a repository's labels
- [ ] `POST		/repos/{owner}/{repo}/labels/{id}` - Create a label
- [ ] `GET		/repos/{owner}/{repo}/labels/{id}` - Get a single label
- [ ] `DELETE	/repos/{owner}/{repo}/labels/{id}` - Delete a label
- [ ] `PATCH	/repos/{owner}/{repo}/labels/{id}` - Update a label
- [ ] `GET		/repos/{owner}/{repo}/milestones` - Get all of a repository's opened milestones
- [ ] `POST		/repos/{owner}/{repo}/milestones` - Create a milestone
- [ ] `GET		/repos/{owner}/{repo}/milestones/{id}` - Get a milestone
- [ ] `DELETE	/repos/{owner}/{repo}/milestones/{id}` - Delete a milestone
- [ ] `PATCH	/repos/{owner}/{repo}/milestones/{id}` - Update a milestone

## repository

- [ ] `POST		/repos/migrate` - Migrate a remote git repository
- [ ] `GET		/repos/search` - Search for repositories
- [ ] `GET		/repos/{owner}/{repo}` - Get a repository
- [ ] `DELETE	/repos/{owner}/{repo}` - Delete a repository
- [ ] `PATCH	/repos/{owner}/{repo}` - Edit a repository's properties. Only fields that are set will be changed
- [ ] `GET		/repos/{owner}/{repo}/archives/{archive}` - Get an archive of a repository
- [ ] `GET		/repos/{owner}/{repo}/assignees` - Return all users that have write access and can be assigned to issues
- [ ] `GET		/repos/{owner}/{repo}/branch_protections` - List branch protections for a repository
- [ ] `POST		/repos/{owner}/{repo}/branch_protections` - Create branch protections for a repository
- [ ] `GET		/repos/{owner}/{repo}/branch_protections/{name}` - Get a specific branch protection for the repository
- [ ] `DELETE	/repos/{owner}/{repo}/branch_protections/{name}` - Delete a specific branch protection for the repository
- [ ] `PATCH	/repos/{owner}/{repo}/branch_protections/{name}` - Edit branch protections for a repository. Only fields that are set will be changed
- [ ] `GET		/repos/{owner}/{repo}/branches/{branch}` - Retrieve a specific branch from a repository, including its effective branch protection
- [ ] `DELETE	/repos/{owner}/{repo}/branches/{branch}` - Delete a specifit branch from a repository
- [ ] `GET		/repos/{owner}/{repo}/collaborators` - List a repository's collaborators 
- [ ] `GET		/repos/{owner}/{repo}/collaborators/{collaborator}` - Check if a user is a collaborator of a repository
- [ ] `PUT		/repos/{owner}/{repo}/collaborators/{collaborator}` - Add a collaborator to a repository
- [ ] `DELETE	/repos/{owner}/{repo}/collaborators/{collaborator}` - Delete a collaborator from a repository
- [ ] `GET		/repos/{owner}/{repo}/collaborators/{collaborator}/permissions` - Get repository permissions for a user
- [ ] `GET		/repos/{owner}/{repo}/commits` - Get a list of all commits from a repository
- [ ] `GET		/repos/{owner}/{repo}/commits/{ref}/status` - Get a commit's combined status, by branch/tag/commit reference
- [ ] `GET		/repos/{owner}/{repo}/commits/{ref}/statuses` - Get a commit's statuses, by branch/tag/commit reference
- [ ] `GET		/repos/{owner}/{repo}/contents` - Gets the metedata of all the entries of the root dir
- [ ] `GET		/repos/{owner}/{repo}/contents/{filepath}` - Gets the metadata and contents (if a file) of an an entry in a repository, or a list of entries if a dir
- [ ] `PUT		/repos/{owner}/{repo}/contents/{filepath}` - Update a file in a repository
- [ ] `POST		/repos/{owner}/{repo}/contents/{filepath}` - Create a file in a repository
- [ ] `DELETE	/repos/{owner}/{repo}/contents/{filepath}` - Delete a file in a repository
- [ ] `POST		/repos/{owner}/{repo}/diffpatch` - Apply diff patch to repository
- [ ] `GET		/repos/{owner}/{repo}/editorconfig/{filepath}` - Get the EditorConfig definitions of a file in a repository
- [ ] `GET		/repos/{owner}/{repo}/forks` - List a repository's forks
- [ ] `POST		/repos/{owner}/{repo}/forks` - Fork a repository
- [ ] `GET		/repos/{owner}/{repo}/git/blobs/{sha}` - Gets the blob of a repository
- [ ] `GET		/repos/{owner}/{repo}/git/commits/{sha}` - Get a single commit from a repository
- [ ] `GET		/repos/{owner}/{repo}/git/commits/{sha}.{diffType}` - Gets a commit's diff or patch
- [ ] `GET		/repos/{owner}/{repo}/git/notes/{sha}` - Get a note corresponding to a single commit from a repository
- [ ] `GET		/repos/{owner}/{repo}/git/refs` - Get specified ref or filtered repository's refs
- [ ] `GET		/repos/{owner}/{repo}/git/refs/{ref}` - Get specified ref or filtered repository's refs
- [ ] `GET		/repos/{owner}/{repo}/git/tags/{sha}` - Gets the tag object of an annotated tag (not lightweight tags)
- [ ] `GET		/repos/{owner}/{repo}/git/trees/{sha}` - Gets the tree of a repository
- [ ] `GET		/repos/{owner}/{repo}/hooks` - List the hooks in a repository
- [ ] `POST		/repos/{owner}/{repo}/hooks` - Create a hook
- [ ] `GET		/repos/{owner}/{repo}/hooks/git` - List the Git hooks in a repository
- [ ] `GET		/repos/{owner}/{repo}/hooks/git/{id}` - Get a Git hook
- [ ] `DELETE	/repos/{owner}/{repo}/hooks/git/{id}` - Delete a Git hook in a repository
- [ ] `PATCH	/repos/{owner}/{repo}/hooks/git/{id}` - Update a Git hook in a repository
- [ ] `GET		/repos/{owner}/{repo}/hooks/{id}` - Get a hook
- [ ] `DELETE	/repos/{owner}/{repo}/hooks/{id}` - Delete a hook in a repository
- [ ] `PATCH	/repos/{owner}/{repo}/hooks/{id}` - Edit a hook in a repository
- [ ] `POST		/repos/{owner}/{repo}/hooks/{id}/tests` - Test a push webhook
- [ ] `GET		/repos/{owner}/{repo}/issue_templates` - Get available issue templates for a repository
- [ ] `GET		/repos/{owner}/{repo}/keys` - List a repository's keys
- [ ] `POST		/repos/{owner}/{repo}/keys` - Add a key to a repository
- [ ] `GET		/repos/{owner}/{repo}/keys/{id}` - Get a repository's key by ID
- [ ] `DELETE	/repos/{owner}/{repo}/keys/{id}` - Delete a key from a repository
- [ ] `GET		/repos/{owner}/{repo}/languages` - Get languages and number of bytes of code written
- [ ] `GET		/repos/{owner}/{repo}/media/{filepath}` - Get a file or it's LFS object from a repository
- [ ] `POST		/repos/{owner}/{repo}/mirror-sync` - Sync a mirrored repository
- [ ] `GET		/repos/{owner}/{repo}/pulls` - List a repo's pull requests
- [ ] `POST		/repos/{owner}/{repo}/pulls` - Create a pull request
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}` - Get a pull request
- [ ] `PATCH	/repos/{owner}/{repo}/pulls/{index}` - Update a pull request. If using deadline only the date will be taken into account, and time of day ignored
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}.{diffType}` - Get a pull request diff or patch
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}/commits` - Get commits for a pull request
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}/merge` - Check if a pull request has been merged
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/merge` - Merge a pull request
- [ ] `DELETE	/repos/{owner}/{repo}/pulls/{index}/merge` - Cancel the scheduled auto merge for the given pull request
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/requested_reviewers` - Create review requests for a pull request
- [ ] `DELETE	/repos/{owner}/{repo}/pulls/{index}/requested_reviewers` - Cancel review requests for a pull request
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}/reviews` - List all reviews for a pull request
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/reviews` - Create a review to a pull request
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}/reviews/{id}` - Get a specific review for a pull request
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/reviews/{id}` - Submit a pending review to a pull request
- [ ] `DELETE	/repos/{owner}/{repo}/pulls/{index}/reviews/{id}` - Delete a specific review from a pull request
- [ ] `GET		/repos/{owner}/{repo}/pulls/{index}/reviews/{id}/comments` - Get the a specific review for a pull request
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/reviews/{id}/dismissals` - Dismiss a review for a pull request
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/reviews/{id}/indismissals` - Cancel to dismiss a review for a pull request
- [ ] `POST		/repos/{owner}/{repo}/pulls/{index}/update` - Merge PR's baseBranch into headBranch
- [ ] `GET		/repos/{owner}/{repo}/raw/{filepath}` - Get a file from a repository
- [ ] `GET		/repos/{owner}/{repo}/releases` - List a repo's releases
- [ ] `POST		/repos/{owner}/{repo}/releases` - Create a release
- [ ] `GET		/repos/{owner}/{repo}/releases/tags/{tag}` - Get a release by tag name 
- [ ] `DELETE	/repos/{owner}/{repo}/release/tags/{tag}` - Delete a release by tag name
- [ ] `GET		/repos/{owner}/{repo}/releses/{id}` - Get a release
- [ ] `DELETE	/repos/{owner}/{repo}/releases/{id}` - Delete a release
- [ ] `PATCH	/repos/{owner}/{repo}/releases/{id}` - Update a release
- [ ] `GET		/repos/{owner}/{repo}/releases/{id}/assets` - List release's attachments
- [ ] `POST		/repos/{owner}/{repo}/releases/{id}/assets` - Create a release attachment
- [ ] `GET		/repos/{owner}/{repo}/releases/{id}/assets/{attechment_id}` - Get a realese attachment
- [ ] `DELETE	/repos/{owner}/{repo}/releases/{id}/assets/{attachment_id}` - Delete a release attachment
- [ ] `PATCH	/repos/{owner}/{repo}/releases/{id}/assets/{attechment_id}` - Update a release attachment
- [ ] `GET		/repos/{owner}/{repo}/reviewers` - List all users that can be requested to review in this repo
- [ ] `GET		/repos/{owner}/{repo}/signing-key.gpg` - Get signing-key.gpg for the given repository
- [ ] `GET		/repos/{owner}/{repo}/stargazers` - List a repo's stargazers
- [ ] `GET		/repos/{owner}/{repo}/statuses/{sha}` - Get a commit's statuses
- [ ] `POST		/repos/{owner}/{repo}/statuses/{sha}` - Create a commit status
- [ ] `GET		/repos/{owner}/{repo}/subscribers` - List a repo's watchers
- [ ] `GET		/repos/{owner}/{repo}/subscription` - Check if the current user is watching a repo
- [ ] `PUT		/repos/{owner}/{repo}/subscription` - Watch a repo
- [ ] `DELETE	/repos/{owner}/{repo}/subscription` - Unwatch a repo
- [ ] `GET		/repos/{owner}/{repo}/tags` - List a repository's tags
- [ ] `POST		/repos/{owner}/{repo}/tags` - Create a new git tag in a repository
- [ ] `GET		/repos/{owner}/{repo}/tags/{tag}` - Get the tag of a repository by tag name
- [ ] `DELETE	/repos/{owner}/{repo}/tags/{tag}` - Delete a repository's tag by name
- [ ] `GET		/repos/{owner}/{repo}/teams` - List a repository's teams
- [ ] `GET		/repos/{owner}/{repo}/teams/{team}` - Check if a team is assigned to a repository
- [ ] `PUT		/repos/{owner}/{repo}/teams/{team}` - Add a team to a repository
- [ ] `DELETE	/repos/{owner}/{repo}/teams/{team}` - Delete a team from a repository
- [ ] `GET		/repos/{owner}/{repo}/times` - List a repo's tracked times
- [ ] `GET		/repos/{owner}/{repo}/topics` - Get a list of topics that a repository has
- [ ] `PUT		/repos/{owner}/{repo}/topics` - Replace list of topics for a repository
- [ ] `PUT		/repos/{owner}/{repo}/topics/{topic}` - Add a topic to a repository
- [ ] `DELETE	/repos/{owner}/{repo}/topics/{topic}` - Delete a topic from a repository
- [ ] `POST		/repos/{owner}/{repo}/transfer` - Tranfer a repo ownership
- [ ] `POST		/repos/{owner}/{repo}/transfer/accept` - Accept a repo transfer
- [ ] `POST		/repos/{owner}/{repo}/transfer/reject` - Reject a repo transfer
- [ ] `POST		/repos/{owner}/{repo}/wiki/new` - Create a wiki page
- [ ] `GET		/repos/{owner}/{repo}/wiki/page/{pageName}` - Get a wiki page
- [ ] `DELETE	/repos/{owner}/{repo}/wiki/page/{pageName}` - Delete a wiki page
- [ ] `PATCH	/repos/{owner}/{repo}/wiki/page/{pageName}` - Edit a wiki page
- [ ] `GET		/repos/{owner}/{repo}/wiki/pages` - Get all wiki pages
- [ ] `GET		/repos/{owner}/{repo}/wiki/revisions/{pageName}` - Get revisions of a wiki page
- [ ] `POST		/repos/template_owner}/{template_repo}/generate` - Create a repository using a template
- [ ] `GET		/repositories/{id}` - Get a repository by id
- [ ] `GET		/topics/search` - Search topics via keyword
- [ ] `POST		/user/repos` - Create a repository

## settings

- [ ] `GET		/settings/api` - Get instance's global settings for api
- [ ] `GET		/settnigs/attachment` - Get instance's global settings for Attachment
- [ ] `GET		/settings/repository` - Get instance's global settings for repositories
- [ ] `GET		/settings/ui` - Get instance's global settings for ui

## user

- [x] `GET		/user` - Get the authenticated user
- [ ] `GET		/user/applications/oauth2` - List the authenticated user's OAuth2 applications
- [ ] `POST		/user/applications/oauth2` - Creates a new OAuth2 application
- [ ] `GET		/user/applications/oauth2/{id}` - Get an OAuth2 application
- [ ] `DELETE	/user/applications/oauth2/{id}` - Delete an OAuth2 application
- [ ] `PATCH	/user/applications/oauth2/{id}` - Update an OAuth2 application, this includes regenerating the client secret
- [x] `GET		/user/emails` - List the authenticated user's email addresses
- [x] `POST		/user/emails` - Add email addresses
- [x] `DELETE	/user/emails` - Delete email addresses
- [ ] `GET		/user/followers` - List the authenticated user's followers
- [ ] `GET		/user/following` - List the users that the authenticated user is following
- [ ] `GET		/user/following/{username}` - Check whether a user is followed by the authenticated user
- [ ] `PUT		/user/following/{username}` - Follow a user
- [ ] `DELETE	/user/following/{username}` - Unfollow a user
- [ ] `GET		/user/gpg_key_token` - Get a token to verify
- [ ] `POST		/user/gpg_key_verify` - Verify a GPG key
- [ ] `GET		/user/gpg_keys` - List the authenticated user's GPG keys
- [ ] `POST		/user/gpg_keys` - Create a GPG key
- [ ] `GET		/user/gpg_keys/{id}` - Get a GPG key
- [ ] `DELETE	/user/gpg_keys/{id}` - Remove a GPG key
- [ ] `GET		/user/keys` - List the authenticated user's public keys
- [ ] `POST		/user/keys` - Create a public key
- [ ] `GET		/user/keys/{id}` - Get a public key
- [ ] `DELETE	/user/keys/{id}` - Delete a public key
- [ ] `GET		/user/repos` - List the repos that the authenticated user owns
- [ ] `POST		/user/repos` - Create a repository
- [ ] `GET		/user/settings` - Get user settings
- [ ] `PATCH	/user/settings` - Update user settings
- [ ] `GET		/user/starred` - List the repos that the authenticated user has starred
- [ ] `GET		/user/starred/{owner}/{repo}` - Check whether the authenticated user is starring a repo
- [ ] `PUT		/user/starred/{owner}/{repo}` - Star a given repo
- [ ] `DELETE	/user/starred/{owner}/{repo}` - Unstar a gives repo Unstar a gives repo
- [ ] `GET		/user/stopwatches` - Get a list of all existing stopwatches
- [ ] `GET		/user/subscriptions` - List repositories watched by the authenticated user
- [ ] `GET		/user/teams` - List all the teams a user belongs to
- [ ] `GET		/user/times` - List the current user's tracked times
- [ ] `GET		/user/search` - Search for a user
- [ ] `GET		/users/{username}` - Get a user
- [ ] `GET		/users/{username}/followers` - List the given user's followers
- [ ] `GET		/users/{username}/following` - List the users that the given user is following
- [ ] `GET		/users/{username}/following/{target}` - Check if one user is following another user
- [ ] `GET		/users/{username}/gpg_keys` - List the given user's GPG keys
- [ ] `GET		/users/{username}/heatmap` - Get a user's heatmap
- [ ] `GET		/users/{username}/keys` - List the given user's public keys
- [ ] `GET		/users/{username}/repos` - List the repos owned by the given user
- [ ] `GET		/users/{username}/starred` - List the repos that the given user has starred
- [ ] `GET		/users/{username}/subscriptions` - List the repositories watched by a user
- [ ] `GET		/users/{username}/tokens` - List the authenticated user's access tokens
- [ ] `POST		/users/{username}/tokens` - Create an access token
- [ ] `DELETE	/users/{username}/tokens/{token}` - Delete an access token

# Testing

In order to run all unit tests, you would first have to provide your gitea api key and your gitea instance url. To do so please copy them into the appropriate `.txt` files in the `rsc/` folder.

Afterwards you can safefly run `cargo test`. If that one fails try running `cargo test -- --nocapture` to see if you have entered the token and url correctly.
