#[macro_use]
extern crate lazy_static;

pub mod client;
pub mod miscellaneous;
pub mod user;
pub mod utils;

lazy_static!
{
    /// Contains the gitea api token for testing purposes.
    pub static ref TOKEN: String = utils::get_token();
}

lazy_static!
{
    /// Contains the gitea instance url for testing purposes.
    pub static ref URL: String = utils::get_url();
}

