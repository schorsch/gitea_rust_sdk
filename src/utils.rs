use std::fs;

/// Read the gitea api token from the `rsc/gitea_api_token.txt` file and return it as a string.
pub fn get_token() -> String
{
    let token = read_file("rsc/gitea_api_token.txt".to_owned()).replace("\n", "");

    if token == "<PUT YOUR TOKEN HERE>"
    {
        println!("Please enter you gitea api token to the 'rsc/gitea_api_token.txt' file.");
        println!("You can create a token at this url: https://<YOUR GITEA INSTANCE>/user/settings/applications\nE.g.: https://codeberg.org/user/settings/applications");
        std::process::exit(1);
    }

    return token;
}

/// Read the gitea instance url from the `rsc/gitea_instance_url.txt` file and return it as a string.
pub fn get_url() -> String
{
    let url = read_file("rsc/gitea_instance_url.txt".to_owned()).replace("\n", "");

    if url == "<PUT YOUR INSTANCE URL HERE>"
    {
        println!("Please enter you gitea instance url to the 'rsc/gitea_instance_url.txt' file.");
        println!("E.g.: https://codeberg.org");
        std::process::exit(1);
    }

    return url;
}

/// Read the content of a given file and return it as a string.
fn read_file(path: String) -> String
{
    return fs::read_to_string(path).expect("Should have been able to read the file");
}

