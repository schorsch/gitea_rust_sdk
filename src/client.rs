use reqwest::header;
use std::result::Result as StdResult;
use thiserror::Error;

/// Error represents all of the possible errors that can happen with the Gitea
/// API. Most of these errors boil down to user error.
#[derive(Error, Debug)]
pub enum Error {
    #[error("error from reqwest: {0:#?}")]
    Reqwest(#[from] reqwest::Error),

    #[error("bad API token: {0:#?}")]
    BadAPIToken(#[from] reqwest::header::InvalidHeaderValue),

    #[error("error parsing/serializing json: {0:#?}")]
    Json(#[from] serde_json::Error),

    #[error("tag not found: {0}")]
    TagNotFound(String),
}

/// A handy alias for Result like `anyhow::Result`.
pub type Result<T> = StdResult<T, Error>;

/// Representation of a reqwest client and a gitea instance url.
#[derive(Clone)]
pub struct Client
{
    pub base_url: String,
    pub cli: reqwest::Client
}

impl Client
{
    /// Create a new client.
    ///
    /// gitea_url: &str
    /// token: &str
    /// application_name: &str
    ///
    /// ```no_run
    /// use gitea_rust_sdk::{client::Client, URL, TOKEN};
    ///
    /// let client = Client::new(&URL, &TOKEN, "schorch/gitea_rust_sdk").unwrap();
    /// ```
    pub fn new(gitea_url: &str, token: &str, application_name: &str) -> Result<Self>
    {
        let mut headers = header::HeaderMap::new();
        let auth = format!("token {}", token);
        let auth = auth.as_str();
 
        headers.insert(header::AUTHORIZATION, header::HeaderValue::from_str(auth)?);
        
        let client = reqwest::Client::builder()
            .user_agent::<String>(application_name.into())
            .default_headers(headers)
            .build()?;
        
        return Ok(Self 
        {
            cli: client,
            base_url: format!("{}/api/v1", gitea_url)
        });
    }
}

