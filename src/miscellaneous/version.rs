use crate::client::Client;
use anyhow::Result;
use serde::{Deserialize, Serialize};

/// The version of Gitea.
/// https://try.gitea.io/api/swagger#model-ServerVersion
#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Version
{
    pub version: String,
}

/// Get the current api version.
///
/// ```rust
/// use gitea_rust_sdk::{client::Client, miscellaneous::version, URL, TOKEN};
/// use regex::Regex;
/// use tokio;
///
/// #[tokio::test]
/// async fn test_version()
/// {
///     let regex = Regex::new(r"[0-9][.][0-9]+[.][0-9].").unwrap();
///     let client = Client::new(&URL,
///                              &TOKEN,
///                              "schorsch/gitea-rust-sdk"
///                  ).unwrap();
///     let version = version::get_version(client).await.unwrap();
///
///     assert!(regex.is_match(&version.version));
/// }
/// ```
pub async fn get_version(client: Client) -> Result<Version>
{
    Ok(client
       .cli
       .get(&format!("{}/version", client.base_url))
       .send()
       .await?
       .error_for_status()?
       .json()
       .await?
    )
}

