use serde::{Deserialize, Serialize};

use crate::client::Client;

/// Abstaction of an email response containing the email address and whether the is verified / the
/// primary email or not
#[derive(Deserialize, Serialize, Clone)]
pub struct Email
{
    pub email: String,
    pub verified: bool,
    pub primary: bool
}

/// Get all registered emails of the currently logged in user
///
/// ```rust
/// use gitea_rust_sdk::{client::Client, user::emails::get_emails, URL, TOKEN};
/// use assert_str::assert_str_eq;
/// use tokio;
///
/// #[tokio::main]
/// async fn main()
/// {
///     let client = Client::new(&URL, &TOKEN, "schorch/gitea_rust_sdk").unwrap();
///     let emails = get_emails(client).await;
///
///     assert_str_eq!(emails[0].email, "rene@schaar.priv.at");
///     assert!(emails[0].verified);
///     assert!(emails[0].primary);
/// }
/// ````
pub async fn get_emails(client: Client) -> Vec<Email>
{
    let emails = client
        .cli
        .get(&format!("{}/user/emails", client.base_url))
        .send()
        .await
        .unwrap()
        .error_for_status()
        .unwrap()
        .json::<Vec<Email>>()
        .await
        .unwrap();

    return emails;
}

/// A struct to post and delete email addresses
#[derive(Serialize)]
struct RequestEmail
{
    emails: [String; 1]
}

/// Add an email address to the currently logged in user
///
/// ```no_run
/// use gitea_rust_sdk::{client::Client, user::emails::add_email, URL, TOKEN};
/// use tokio;
///
/// #[tokio::main]
/// async fn main()
/// {
///     let client = Client::new(&URL, &TOKEN, "schorch/gitea_rust_sdk").unwrap();
///     let email = add_email(client.clone(), "foo@bar.at").await;
/// }
/// ```
pub async fn add_email(client: Client, email: &str) -> Email
{
    let email = client
        .cli
        .post(&format!("{}/user/emails", client.base_url))
        .json(&RequestEmail{emails: [email.to_owned()]})
        .send()
        .await
        .unwrap()
        .error_for_status()
        .unwrap()
        .json::<Vec<Email>>()
        .await
        .unwrap()[0]
        .clone();
    
    return email;
}

/// Delete an email addres of the currently logged in user
///
/// ```no_run
/// use gitea_rust_sdk::{client::Client, user::emails::delete_email, URL, TOKEN};
/// use tokio;
///
/// #[tokio::main]
/// async fn main()
/// {
///     let client = Client::new(&URL, &TOKEN, "schorch/gitea_rust_sdk").unwrap();
///     let success = delete_email(client, "foo@bar.at").await;
/// }
/// ```
pub async fn delete_email(client: Client, email: &str) -> bool
{
    let response = client
        .cli
        .delete(&format!("{}/user/emails", client.base_url))
        .json(&RequestEmail{emails: [email.to_owned()]})
        .send()
        .await
        .unwrap()
        .error_for_status();

    return match response
    {
        Ok(_) => true,
        Err(e) => {
            println!("Error handling delete_email response: {}", e);
            false
        }
    }
}

#[cfg(test)]
mod tests
{
    use crate::{client::Client, URL, TOKEN, user::emails::{add_email, delete_email}};
    use assert_str::assert_str_eq;

    #[tokio::test]
    async fn test_add_and_delete_email()
    {
        let client = Client::new(&URL, &TOKEN, "schorsch/gitea_rust_sdk").unwrap();
        // Create an email to delete
        let email = add_email(client.clone(), "foo@bar.at").await;
        // Delete this particular email
        let success = delete_email(client, "foo@bar.at").await;

        // The email address should be "foo@bar.at" while it should neither be verified, nor the
        // primary one.
        assert_str_eq!(email.email, "foo@bar.at");
        assert!(!email.verified);
        assert!(!email.primary);

        // The deletion of the email should be successful :)
        assert!(success);
    }
}

