use crate::client::Client;
use serde::Deserialize;

pub mod emails;

/// Representation of a gitea user.
#[derive(Deserialize)]
pub struct User
{
    pub id: i64,

    pub username: String,
    pub full_name: String,
    pub email: String,

    pub avatar_url: String,
    pub language: String,
    pub is_admin: bool,

    pub last_login: String,
    pub created: String,

    pub restricted: bool,
    pub active: bool,
    pub prohibit_login: bool,

    pub location: String,
    pub website: String,
    pub description: String,
    pub visibility: String,

    pub followers_count: u64,
    pub following_count: u64,
    pub starred_repos_count: u64
}

/// Get information about the authenticated user
///
/// ```rust
/// use anyhow::Result;
/// use assert_str::assert_str_eq;
/// use gitea_rust_sdk::{client::Client, user::User};
///    
///    #[tokio::test]
///    async fn test_get_user()
///    {
///        let client = Client::new(&URL,
///                                 &TOKEN,
///                                 "schorsch/gitea-rust-sdk"
///                     ).unwrap();
///        let user = user::get_user(client).await;
///
///        assert_eq!(user.id, 30501);
///        assert_str_eq!(user.username, "schorsch")
///    }
/// ```
pub async fn get_user(client: Client) -> User
{
    let user = client
        .cli
        .get(&format!("{}/user", client.base_url))
        .send()
        .await
        .unwrap()
        .error_for_status()
        .unwrap()
        .json::<User>()
        .await
        .unwrap();

    return user;
}

